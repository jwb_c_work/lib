import { expect } from "chai";
import { testSettings } from "../../test/main";
import * as _ from 'lodash';


describe("Samples", () => {

    describe("Addition", () => {
        it("Should be able to add", () => {
            expect(2 + 2).to.equal(4)
        });
    });

    describe("Array", () => {
        it("Should be able to check array", () => {
            expect(_.map([{ ID: 5, Id: 5, Title: "Test Item" }], i => { return { Title: i.Title } })).to.deep.include({ Title: "Test Item" })
        });

        it("Should be able to check nested values", () => {
            let data = [{
                Title: "Test",
                Author: {
                    Id: 5,
                    Title: "Me"
                }
            }, {
                Title: "Other",
                Author: {
                    Id: 3,
                    Title: "So"
                }
            }]

            expect(_.first(data)).to.have.keys(["Title", "Author"]).that.has.property("Author").that.has.keys("Id", "Title")
        });
    });

    describe("Objects", () => {
        it("Should be able to check values", () => {
            expect({
                Title: "From Lib",
                Other: "Test2"
            }).to.include({ "Title": "From Lib" })
        })
    })
});