import { expect } from "chai";
import { ListService } from "../";
import { testSettings } from "../../test/main";
import * as _ from 'lodash';

describe("ListService", () => {

    let site: string;
    let listTitle: string;

    before(() => {
        site = testSettings.sp.webUrl;
        listTitle = "TestList";
    });

    describe("getItems", () => {
        it("Should be able to get items", () => {
            return ListService.getItems(site, listTitle).then(items => {
                expect(_.map(items, i => _.pick(i, ['Title']))).to.deep.include({ Title: "Test Item" })
            })
        });

        it("Should be able to get top items", () => {
            return ListService.getItems(site, listTitle, {
                top: 2
            }).then(items => {
                expect(items.length).to.equal(2);
            })
        });

        it("Should be able to query on Title", () => {
            return ListService.getItems(site, listTitle, {
                filter: "Title eq 'Other'"
            }).then(items => {
                expect(_.pick(_.first(items), 'Title')).to.deep.eq({ Title: 'Other' });
            })
        })

        it("Should be able to select Columns", () => {
            return ListService.getItems(site, listTitle, {
                select: ["Title", "Modified", "FileRef"]
            }).then(items => {
                expect(_.map(items, i => _.pick(i, ["Title", "Modified", "FileRef"]))).all.keys("Title", "Modified", "FileRef")
            })
        })

        it("Should be able to select Columns that expand", () => {
            return ListService.getItems(site, listTitle, {
                fields: ["Title", "Modified", "Author/Id", "Author/Title"]
            }).then(items => {
                expect(_.first(items))
                    .to.contain.keys(["Title", "Modified", "Author"])
                    .that.has.property("Author")
                    .that.contain.keys("Id", "Title")
            })
        })
    });

    describe("addItem", () => {
        it("Should be able to add an item", () => {
            return ListService.addItem(site, listTitle, {
                Title: "From Lib"
            }).then(item => {
                expect(item).to.include({ "Title": "From Lib" })
            })
        })
    })

    describe("addItems", () => {
        let items = [{
            Title: "Add Mult 1"
        },
        {
            Title: "Add Mult 2"
        }]

        let itemsChunk = [
            { Title: "Chunk 1" },
            { Title: "Chunk 2" },
            { Title: "Chunk 3" },
            { Title: "Chunk 4" },
            { Title: "Chunk 5" },
            { Title: "Chunk 6" },
            { Title: "Chunk 7" },
            { Title: "Chunk 8" },
            { Title: "Chunk 9" },
            { Title: "Chunk 10" },
            { Title: "Chunk 11" },
            { Title: "Chunk 12" }]

        it("Should be able to add multiple items", () => {
            return ListService.addItems(site, listTitle, items)
                .then(itemsCreated => {
                    expect(itemsCreated).to.equal(items.length)
                })
        })

        it("Should be able to add items large than the chunk size", () => {
            return ListService.addItems(site, listTitle, itemsChunk)
                .then(itemsCreated => {
                    expect(itemsCreated).to.equal(itemsChunk.length)
                })
        })
    })
});
