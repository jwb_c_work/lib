import { expect } from "chai";
import { SiteService } from "../";
import { testSettings } from "../../test/main";
import * as _ from 'lodash';
import { Web } from "@pnp/sp";

describe("SiteService", () => {

    let site: string;
    let listTitle: string;

    before(() => {
        site = testSettings.sp.webUrl;
    });

    describe("createListCtx", () => {
        it("Should create a list via Ctx", () => {
            return SiteService.createList(site, {
                title: "MyTestListCtx"
            }).then(listTitle => {
                expect(listTitle).to.equal("MyTestListCtx");                
            })
        })

        it("Should create a list via Ctx with Content Types", () => {
            return SiteService.createList(site, {
                title: "MyCustomTestListCtx",
                enableContentTypes: true,
                contentTypes: [
                    "CustomSiteContentType"
                ]
            }).then(listTitle => {
                expect(listTitle).to.equal("MyCustomTestListCtx");     
            })
        })
    })

    describe("createListPnP", () => {
        it("Should create a list via PnP", () => {
            return SiteService.createListPnP(site, {
                title: "MyTestList"
            }).then(list => {
                expect(list.data.Title).to.equal("MyTestList");                
            })
        })

        it("Should create a list via PnP with Content Types", () => {
            return SiteService.createListPnP(site, {
                title: "MyCustomTestList",
                enableContentTypes: true,
                contentTypes: [
                    "CustomSiteContentType"
                ]
            }).then(list => {
                expect(list.data.Title).to.equal("MyCustomTestList");      
            })
        })
    })

    after(() => {
        let web = new Web(site);

        //clean up
        return Promise.all([
            web.lists.getByTitle("MyTestList").delete(),            
            web.lists.getByTitle("MyCustomTestList").delete(),
            web.lists.getByTitle("MyTestListCtx").delete(),
            web.lists.getByTitle("MyCustomTestListCtx").delete()
        ])
    })
});
