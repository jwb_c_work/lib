import { Web } from "@pnp/sp";
import { QueryOptions } from "./Options";
import QueryBuilder from "./QueryBuilder";
import * as _ from "lodash";
import { run } from "./ClientContextTS";

export class ListService {

    /**
     * Gets an array of items from a SharePoint list
     * @param siteUrl Site where list is located
     * @param listTitle List to get items from
     * @param options QueryOptions for items
     */
    public static getItems<T>(siteUrl: string, listTitle: string, options: QueryOptions = {}): Promise<Array<T>> {

        let web = new Web(siteUrl);
        let listItems = web.lists.getByTitle(listTitle).items;

        return QueryBuilder.build(listItems, options).get().then(items => {
            if (options.take != null) {
                if (options.take > 0) {
                    items = _.takeRight(items, options.take)
                }
            }

            return items as Array<T>
        });
    }

    /**
     * Creates an item using JSOM
     * @param siteUrl Site where list is located
     * @param listTitle List to add item to
     * @param itemData Item data that will be added
     */
    public static addItem(siteUrl: string, listTitle: string, itemData: any): Promise<any> {
        var clientContext = new SP.ClientContext(siteUrl);

        var contentTypeGet = new Promise(r => r())

        // if (itemData.ContentTypeName != null && itemData.ContentTypeName != "") {
        //     contentTypeGet = service._getListContentTypes(site, listTitle).then(function (listContentTypes) {
        //         var itemCT = _.findWhere(listContentTypes, { Name: itemData.ContentTypeName })

        //         itemData.ContentTypeId = itemCT.Id
        //     });
        // }

        return contentTypeGet.then(function () {
            var listItem = ListService._addItemWithContext(clientContext, listTitle, itemData)
            return run(clientContext).then(function () {
                return listItem.get_fieldValues();
            })
        })
    }

    /**
     * Creates items using JSOM
     * @param siteUrl Site where list is located
     * @param listTitle List to add items to
     * @param items Items that will be added
     */
    public static addItems(siteUrl: string, listTitle: string, items: Array<any>): Promise<number> {
        //Set up variables
        let clientContext = new SP.ClientContext(siteUrl);
        let itemWork = new Promise(r => r())
        let max = 5;

        let chunks = _.chunk(items, max)

        let itemCount = 0;

        //Process each chunk
        chunks.forEach(function (chunkItems) {
            //Use promises to make sure one chunk is processed at a time
            itemWork = itemWork.then(() => {
                //Process each item
                chunkItems.forEach((item) => {
                    itemCount++;
                    ListService._addItemWithContext(clientContext, listTitle, item)
                })
                //Execute a call for each chunk
                return run(clientContext);
            })
        });

        //Return the final chunk promise
        return itemWork.then(function () {
            return itemCount;
        })
    }

    /**
     * Internal functiont to set up items
     * @param clientContext JSOM context to load item to
     * @param listTitle List to add item to
     * @param itemData Item data to set up
     */
    private static _addItemWithContext(clientContext: SP.ClientContext, listTitle: string, itemData: any) {
        var web = clientContext.get_web();
        var list = web.get_lists().getByTitle(listTitle);

        var itemCreateInfo = new SP.ListItemCreationInformation();

        if (itemData.UnderlyingType == "Folder") {
            itemCreateInfo.set_underlyingObjectType(SP.FileSystemObjectType.folder);

            if (itemData.LeafName != null && itemData.LeafName != "") {
                itemCreateInfo.set_leafName(itemData.LeafName);
            }
        }

        if (itemData.FolderUrl != null && itemData.FolderUrl != "") {
            itemCreateInfo.set_folderUrl(itemData.FolderUrl)
        }

        var listItem = list.addItem(itemCreateInfo);

        if (itemData.ContentTypeId != null) {
            listItem.set_item("ContentTypeId", itemData.ContentTypeId);
        }

        _.keys(itemData).forEach(function (key) {
            if (key != "ContentTypeName" && key != "ContentTypeId"
                && key != "LeafName" && key != "UnderlyingType" && key != "FolderUrl") {
                listItem.set_item(key, itemData[key]);
            }
        })

        listItem.update();
        clientContext.load(listItem);

        return listItem;
    }
}