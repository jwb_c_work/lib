import { SharePointQueryableCollection } from "@pnp/sp";
import { QueryOptions } from "./Options";
import * as _ from "lodash";

export default class QueryBuilder {
    static build(inputCollection: SharePointQueryableCollection, options: QueryOptions): SharePointQueryableCollection {
        let collection = inputCollection;

        if (options.top != null) {
            collection = collection.top(options.top)
        }

        if (options.filter != null) {
            collection = collection.filter(options.filter)
        }

        if (options.select != null) {
            collection = collection.select(options.select.join(","))
        }

        if (options.fields != null) {

            let fieldsString = options.fields.join(",");
            collection = collection.select(fieldsString);

            let expandFields = [];

            options.fields.forEach(function (field) {
                if (field.indexOf("/") >= 0) {
                    expandFields.push(field.split("/")[0]);
                }
            })

            if (expandFields.length > 0) {
                let expandFieldsString = _.uniq(expandFields).join(",");
                collection = collection.expand(expandFieldsString);
            }
        }

        return collection;
    }
}
