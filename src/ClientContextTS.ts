// export default class ClientContextTS extends SP.ClientContext {
//     public run(): Promise<any> {
//         return new Promise((resolve, reject) => {
//             this.executeQueryAsync(resolve, reject)
//         })
//     }
// }

export function run(ctx: SP.ClientContext): Promise<any>{
    return new Promise((resolve, reject) => {
        ctx.executeQueryAsync(resolve, reject)
    })
}