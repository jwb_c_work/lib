import { Web, ContentTypeAddResult, ListAddResult } from "@pnp/sp";
import { ListOptions } from "./Options";
import QueryBuilder from "./QueryBuilder";
import * as _ from "lodash";
import { run } from "./ClientContextTS";

export class SiteService {

    /**
     * Creates a list using the JSOM library
     * @param siteUrl Site to create the list at
     * @param listOptions Options for the list
     */
    static createList(siteUrl: string, listOptions: ListOptions): Promise<string> {

        let clientContext = new SP.ClientContext(siteUrl);

        var contentTypes = clientContext.get_web().get_availableContentTypes()

        if (listOptions.enableContentTypes) {
            clientContext.load(contentTypes);
        }

        return run(clientContext).then(() => {
            var contentTypesArray = contentTypes.get_data();
            //Set list properties
            var listCreationInfo = new SP.ListCreationInformation();

            listCreationInfo.set_title(listOptions.title);
            listCreationInfo.set_templateType(listOptions.template != null ? listOptions.template : 100);
            listCreationInfo.set_description(listOptions.description != null ? listOptions.description : "");

            var list = clientContext.get_web().get_lists().add(listCreationInfo);

            list.set_contentTypesEnabled(listOptions.enableContentTypes);

            list.update();

            if (listOptions.enableContentTypes && listOptions.contentTypes != null) {
                listOptions.contentTypes.forEach(function (listContentType) {
                    if (listContentType != null && listContentType != "") {
                        var contentType = _.find(contentTypesArray, function (ct) { return ct.get_name() == listContentType })
                        if (contentType != null) {
                            list.get_contentTypes().addExistingContentType(contentType);
                        }
                    }
                });
            }

            clientContext.load(list);

            return run(clientContext).then(() => {
                return list.get_title();
            })
        })
    }

    /**
     * Uses the PnP Library to create a list
     * @param siteUrl Site to create the list at
     * @param listOptions Options for the list
     */
    static createListPnP(siteUrl: string, listOptions: ListOptions): Promise<ListAddResult> {
        return new Promise((resolve, reject) => {
            let web = new Web(siteUrl)

            web.lists.add(
                listOptions.title,
                listOptions.description,
                listOptions.template,
                listOptions.enableContentTypes)
                .then(listResult => {
                    if (listOptions.enableContentTypes && listOptions.contentTypes != null) {

                        web.contentTypes
                            .select("Name, Id")
                            .filter(_.map(listOptions.contentTypes, ct => `Name eq '${ct}'`).join(" or "))
                            .get().then(foundContentTypes => {

                                let contentTypesToAdd: Array<Promise<ContentTypeAddResult>> = [];

                                foundContentTypes.forEach(ct => {
                                    contentTypesToAdd.push(listResult.list.contentTypes.addAvailableContentType(ct.Id.StringValue))
                                })

                                Promise.all(contentTypesToAdd).then(() => {
                                    resolve(listResult)
                                }, reject)
                            }, reject)
                    } else {
                        resolve(listResult)
                    }

                })
        })
    }
}