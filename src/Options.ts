export interface QueryOptions {
    /**
     * Gets the top number of results
     */
    top?: number,
    take?: number,
    /**
    * List of properties to select
    */
    select?: Array<string>,
    /**
    * List of properties to select
    */
    expand?: Array<string>,
    /**
    * List of properties to select, will auto expand properties with '/'
    */
    fields?: Array<string>,
    /**
     * The filter to execute against the collection
     */
    filter?: string,
    sort?: string
}

export interface ListOptions{
    title: string,
    description?: string,
    template?: number,
    enableContentTypes?: boolean,
    contentTypes?: Array<string>
}