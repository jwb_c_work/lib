declare var require: (s: string) => any;
import { SPFetchClient } from "../src/node/spfetchclient";
import { Web, sp } from "@pnp/sp";
import * as chai from "chai";
import * as chaiAsPromised from "chai-as-promised";
import * as chaiLike from "chai-like";
import * as chaiThings from "chai-things";
import "mocha";
import { JsomNode, IJsomNodeInitSettings } from "sp-jsom-node";

chai.use(chaiAsPromised);
chai.use(chaiLike);
chai.use(chaiThings);

declare var process: any;

export interface ISettingsTestingPart {
    enableWebTests: boolean;
    graph?: {
        id: string;
        secret: string;
        tenant: string;
    };
    sp?: {
        webUrl?: string;
        id: string;
        notificationUrl: string | null;
        secret: string;
        url: string;
    };
}

export interface ISettings {
    testing: ISettingsTestingPart;
}

let settings: ISettings = null;
let site: string = null;
let mode: string = null;
let clientId: string = null;
let secret: string = null;
let testUrl: string = null;

process.argv.forEach((s: string) => {
    if (/^--vsp-test-site/.test(s)) {
        site = s.split("=")[1];
    }
    if (/^--vsp-test-mod/.test(s)) {
        mode = s.split("=")[1];
    }

    if (/^--vsp-test-clientid/.test(s)) {
        clientId = s.split("=")[1];
    }

    if (/^--vsp-test-secret/.test(s)) {
        secret = s.split("vsp-test-secret=")[1];
    }
    if (/^--vsp-test-url/.test(s)) {
        testUrl = s.split("=")[1];
    }
});

switch (mode) {
    case "bitbucket":
        settings = {
            testing: {
                enableWebTests: true,
                sp: {
                    id: clientId,
                    secret: secret,
                    url: testUrl,
                    notificationUrl: null,
                },
            },
        };
        break;
    case "local":
    default:
        settings = require("../../settings");
        break;
}

function getGUID(): string {
    let d = new Date().getTime();
    const guid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
        const r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === "x" ? r : (r & 0x3 | 0x8)).toString(16);
    });
    return guid;
}

function stringIsNullOrEmpty(s: string): boolean {
    return typeof s === "undefined" || s === null || s.length < 1;
}

function combinePaths(...paths: string[]): string {

    return paths
        .filter(path => !stringIsNullOrEmpty(path))
        .map(path => path.replace(/^[\\|\/]/, "").replace(/[\\|\/]$/, ""))
        .join("/")
        .replace(/\\/g, "/");
}

function spTestSetup(ts: ISettingsTestingPart): Promise<void> {

    return new Promise((resolve, reject) => {



        if (site && site.length > 0) {

            let jsomNodeOptions: IJsomNodeInitSettings = {
                siteUrl: site,
                authOptions: {
                    clientId: ts.sp.id,
                    clientSecret: ts.sp.secret
                }
            };

            (new JsomNode(jsomNodeOptions)).init();

            // we have a site url provided, we'll use that
            sp.setup({
                sp: {
                    fetchClientFactory: () => {
                        return new SPFetchClient(site, ts.sp.id, ts.sp.secret);
                    },
                },
            });

            ts.sp.webUrl = site;
            return resolve();
        }

        sp.setup({
            sp: {
                fetchClientFactory: () => {
                    return new SPFetchClient(ts.sp.url, ts.sp.id, ts.sp.secret);
                },
            },
        });

        // create the web in which we will test
        const d = new Date();
        const g = getGUID();

        sp.web.webs.add(`Testing ${d.toDateString()}`, g).then(() => {

            const url = combinePaths(ts.sp.url, g);

            // set the testing web url so our tests have access if needed
            ts.sp.webUrl = url;

            // re-setup the node client to use the new web
            sp.setup({

                sp: {
                    // headers: {
                    //     "Accept": "application/json;odata=verbose",
                    // },
                    fetchClientFactory: () => {
                        return new SPFetchClient(url, ts.sp.id, ts.sp.secret);
                    },
                },
            });

            resolve();

        }).catch(reject);
    });


}

export let testSettings: ISettingsTestingPart = settings.testing;

before(function (done: MochaDone) {

    // this may take some time, don't timeout early
    this.timeout(90000);

    // establish the connection to sharepoint
    if (settings.testing.enableWebTests) {

        spTestSetup(settings.testing).then(_ => done()).catch(e => {

            console.log("Error creating testing sub-site: " + JSON.stringify(e));
            done(e);
        });
    }
});