//******************************************************************************
//* build.js
//* 
//* Defines a custom gulp task for compiling TypeScript source code into
//* js files.  It outputs the details as to what it generated to the console.
//******************************************************************************

const gulp = require("gulp"),
    replace = require('gulp-replace'),
    pkg = require("../package.json"),
    exec = require('child_process').exec,
    path = require("path"),
    pump = require('pump'),
    fs = require("fs"),
    ts = require("gulp-typescript"),
    tsProject = ts.createProject("tsconfig.json");
    testProject = ts.createProject("test/tsconfig.json");

const tscPath = ".\\node_modules\\.bin\\tsc";

// give outselves a single reference to the projectRoot
const projectRoot = path.resolve(__dirname, "../..");


/**
 * Does the main build that is used by package and publish
 */
gulp.task("build", ["clean"], (done) => {

    return tsProject.src()
        .pipe(tsProject())
        .js.pipe(gulp.dest("build/js"));
});

gulp.task("build:def", (done) => {

    return tsProject.src()
        .pipe(tsProject())
        .dts.pipe(gulp.dest("build/def"));
});

gulp.task("build:test", ["clean"], (done) => {
    return testProject.src()
    .pipe(testProject())
    .js.pipe(gulp.dest("testing"));
});
