//******************************************************************************
//* build.js
//* 
//* Defines a custom gulp task for compiling TypeScript source code into
//* js files.  It outputs the details as to what it generated to the console.
//******************************************************************************

const gulp = require("gulp"),
    yargs = require('yargs').argv,
    mocha = require("gulp-mocha");

const tscPath = ".\\node_modules\\.bin\\tsc";



/**
 * Does the main build that is used by package and publish
 */


// gulp.task("test", ["clean", "build:test"], () => {

//     let paths = [];

//     paths.push("./testing/test/main.js");
//     paths.push("./testing/**/*.test.js");

//     gulp.src(paths).pipe(mocha())
//         .once('error', function () {
//             process.exit(1);
//         })
//         .once('end', function () {
//             process.exit();
//         });
// });

gulp.task("test", ["clean", "build:test"], () => {

    let paths = [];

    // paths.push("./src/jsom/1033.init.js");
    // paths.push("./src/jsom/MicrosoftAjax2.js");
    // paths.push("./src/jsom/SP.Core.js");
    // paths.push("./src/jsom/SP.Runtime.js");
    // paths.push("./src/jsom/SP.js");
    //paths.push("./src/jsom/SP.Init.js");
    //paths.push("./src/jsom/SP.Publishing.js");
    //paths.push("./src/jsom/SP.Taxonomy.js");

    paths.push("./testing/test/main.js");
    paths.push("./testing/**/Sample.test.js");
    paths.push("./testing/**/SiteService.test.js");
    paths.push("./testing/**/ListService.test.js");
    //paths.push("./testing/**/*.test.js");


    var siteUrl = "https://vanadev.sharepoint.com/sites/DeveloperPlayground/0cfb6da2-2439-48ab-9067-256e7c9f7fed"

    gulp.src(paths)
        .pipe(mocha({
            timeout: 10000,
            "vsp-test-site": siteUrl
        }))
        .once('error', function () {
            process.exit(1);
        })
        .once('end', function () {
            process.exit();
        });
});

gulp.task("test:bitbucket", ["clean", "build:test"], () => {

    let paths = [];

    // paths.push("./src/jsom/1033.init.js");
    // paths.push("./src/jsom/MicrosoftAjax2.js");
    // paths.push("./src/jsom/SP.Core.js");
    // paths.push("./src/jsom/SP.Runtime.js");
    // paths.push("./src/jsom/SP.js");
    //paths.push("./src/jsom/SP.Init.js");
    //paths.push("./src/jsom/SP.Publishing.js");
    //paths.push("./src/jsom/SP.Taxonomy.js");

    paths.push("./testing/test/main.js");
    paths.push("./testing/**/Sample.test.js");
   // paths.push("./testing/**/SiteService.test.js");
    paths.push("./testing/**/ListService.test.js");
    //paths.push("./testing/**/*.test.js");


    var siteUrl = "https://vanadev.sharepoint.com/sites/DeveloperPlayground/0cfb6da2-2439-48ab-9067-256e7c9f7fed"

    console.log(yargs.clientId);

    gulp.src(paths)
        .pipe(mocha({
            timeout: 10000,
            "vsp-test-site": siteUrl,
            "vsp-test-mode": "bitbucket",
            "vsp-test-clientid": yargs.clientId,
            "vsp-test-secret": yargs.secret,
            "vsp-test-url": yargs.url
        }))
        .once('error', function () {
            process.exit(1);
        })
        .once('end', function () {
            process.exit();
        });
});